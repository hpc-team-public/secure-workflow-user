#!/bin/bash

user_name=$1
secure_node=$2

dir=$(pwd)
me=$(whoami)

# Variaböles to further control the script
partition="hendrik"

# Create first Data Container. We use this to place our input data. We mount this container /mnt/secure_workflow. The name of the container is test and we assign a size of 30 MB to it.
sudo mkdir -p /mnt/secure_workflow/test
sudo ../create_data_container.sh test /mnt/secure_workflow 30
# CHange ownership so that we can write to it in user space
sudo chown $me /mnt/secure_workflow/test

# Go into our mount directory and download an example png
cd /mnt/secure_workflow/test/
wget https://w7.pngwing.com/pngs/76/223/png-transparent-team-spirit-cohesion-together-generations-hands-team-teamwork-thumbnail.png
mv png-transparent-team-spirit-cohesion-together-generations-hands-team-teamwork-thumbnail.png source.png

# CHange back to our original directory
cd $dir
# And umount our Container and close the device.
sudo ../create_data_container_umount.sh test /mnt/secure_workflow

# Create our output container. First we create our corresponding mountpoint. 
sudo mkdir -p /mnt/secure_workflow/testoutput
# Here we create our container, it will be called testoutput, it will be mounted under /mnt/secure_workflow and also has a size of 30 MB
sudo ../create_data_container.sh testoutput /mnt/secure_workflow 30
# Here we close the container with the name testoutput again and umount it from /mnt/secure_workflow
sudo ../create_data_container_umount.sh testoutput /mnt/secure_workflow

# Create a fresh pair of keys in order to generate a PEM file to encrypt our singularity Container
ssh-keygen -t rsa -b 4096 -m pem -N "" -f ./rsa
# Generate a PEM file from it
ssh-keygen -f ./rsa.pub -e -m pem >rsa_pub.pem
#Just rename the PEM file
mv rsa rsa_pri.pem
# Build the singularity container and use our before created PEM file to encrypt it
sudo singularity build --pem-path=rsa_pub.pem encrypted.sif recipe

# Test wthether the encrypted build was successfull or not. Doing this by simply running it:
if !  [[ $(singularity run  --pem-path=./rsa_pri.pem encrypted.sif bash -c "echo Hello" | grep Hello) ]]; then
    echo "Building the Container didn't work."
    exit 1 ;
fi
# Create working directory in our $HOME
ssh $user_name@gwdu101.gwdg.de 'mkdir secure_workflow'

# Copy our two encrypted Data Containers as well as our encrypted Singularity Container into our $HOME. This is save because the containers are encrypted. 
scp test.img $user_name@transfer-scc.gwdg.de:/usr/users/$user_name/secure_workflow
scp testoutput.img $user_name@transfer-scc.gwdg.de:/usr/users/$user_name/secure_workflow
scp encrypted.sif $user_name@transfer-scc.gwdg.de:/usr/users/$user_name/secure_workflow

# Rewriting our run script to be executed on sgiz001 in partition hendrik and on the specified node from above (look at call arguments)
sed -i "s/<secure_partition>/$partition/g" ../run_dummy_job.sh
sed -i "s/<prepared_node>/$secure_node/g" ../run_dummy_job.sh

# Upload run_dummy script and sbatch it. Now we have a session on the secured node and SLurm will let us ssh onto it. Only hereby we can asure that we have the node exclusively so no one but us can access the node
scp ../run_dummy_job.sh $user_name@gwdu101:/usr/users/$user_name/secure_workflow

# Submit dummy job and store jobid
jobid=$(ssh $user_name@gwdu101 '/opt/slurm/bin/sbatch secure_workflow/run_dummy_job.sh' | awk '{print $4}')

#Wait until job started and we can ssh on the secured node. Longer waiting times can occur if the node is currently used by another user
while [[ -z $(ssh $user_name@gwdu101 "/opt/slurm/bin/squeue -j $jobid | awk '{if(NR>1)print}' |  awk '{print $5}' | grep R") ]]; do echo "Waiting for Dummy Job to start" && sleep 10 ; done

# Doing a secure_upload in order to securely upload our keys. Keys MUST NOT be placed in your $HOME only /keys on a secured node is a secure place. Please always use secure_upload.sh to upload keys! We here specify explicitly the key name to upload and our hpc username
../secure_upload.sh $secure_node test.key $user_name
../secure_upload.sh $secure_node testoutput.key $user_name
../secure_upload.sh $secure_node rsa_pri.pem $user_name

# Rewriting our run script to be executed on sgiz001 in partition hendrik
sed -i "s/<secure_partition>/$partition/g" ./run.sh
sed -i "s/<prepared_node>/$secure_node/g" ./run.sh

# JUST FOR ME hnolte1 BECAUSE I HAVE A NFS HOME!!
#ssh $user_name@$secure_node "cp /usr/users/$user_name/secure_workflow/test.img /local/"
#ssh $user_name@$secure_node "cp /usr/users/$user_name/secure_workflow/testoutput.img /local/"

# Do mount containers explicitly
# Is currently done in run.sh This is another possiblity. Useful if you have several JObs using the same data
#ssh -tt $user_name@$secure_node 'mount_data test'
#ssh -tt $user_name@$secure_node 'mount_data testoutput'

# Now we upload our run script. Here we do the actual computation which we originally wanted to do. 
scp run.sh $user_name@gwdu101:/usr/users/$user_name/secure_workflow
# Save the jobid to monitor the end of the job. This job is very short, thus this works.
jobid=$(ssh $user_name@gwdu101 '/opt/slurm/bin/sbatch secure_workflow/run.sh' | awk '{print $4}')

#Wait until job started and we can ssh on the secured node. Longer waiting times can occur if the node is currently used by another user
while [[ $(ssh $user_name@gwdu101 "/opt/slurm/bin/squeue -j $jobid | awk '{if(NR>1)print}'") ]]; do echo "Waiting. Job is executing." && sleep 10 ; done

#  UMOunt explicitly
# Check above the mount_data command
#ssh -tt $user_name@$secure_node 'umount_data test'
#ssh -tt $user_name@$secure_node 'umount_data testoutput'

# JUST fpr me for my NFS-HOME
#ssh $user_name@$secure_node "cp /local/testoutput.img /usr/users/$user_name/secure_workflow/"

# Making some cleanup, since we copied our data on the local SSD. Should rather be part of the run_script. No necessary if you use your $HOME
#ssh -tt $user_name@$secure_node 'rm /local/test.img'
#ssh -tt $user_name@$secure_node 'rm /local/testoutput.img'

# Download result, but before delete old empty container
sudo rm testoutput.img
scp $user_name@transfer-scc.gwdg.de:/usr/users/$user_name/secure_workflow/testoutput.img .

# Mount Output COntainer under /mnt/secure_workflow for you to use it ;)
../mount_data.sh testoutput /mnt/secure_workflow

echo "Please do ../umount_data.sh testoutput /mnt/secure_workflow afterwards to securely umount you result data container"
