# prerequisites

Singularity > 3.4.0

Cryptsetup > 2.0.0

GWDG-Account activated for Scientific Computing

A Secure Node from your Admin

# Instructions to Execute this Example

**Before starting with this Example, please read the Readme to the Project**

In order to run this example **on your own (local) machine** you need to call it like this after your admin of trust provided ou with an secure node:
```
./execute_me.sh <hpc_username> <secure_node>
```
In the end you can do an:
```
ls /mnt/secure_workflow/testoutput/
```
and you will see your `dest.jpg` you created in your job.

# Quick Explanation of this Example

1) This example creates one LUKS data container, downloads a png into it and renames it to `source.png`. A second LUKS data container is used to store our output data.
2) An encrypted Singularity Container is created, where we want to perform our computations in, in this case we will resize an png image to 20% and store it as an jpg with imagemagick.
3) We upload our encrypted containers, both data and singularity into our $HOME. This is save since They are encrypted.
4) We configure our `run.sh` and `run_dummy_job.sh` that it uses the proper partition and node. The node is the same as the above mentioned `<secure_node>`.
5) ** We use the `secure_upload.sh` in order to securely upoad our keys to the node we exclusively reserved.**
6) We mount our data containers by using the `mount_data <data_container_name>` command
7) We run our computations
8) We umount our data by using the `umount_data <data_container_name>` command.
9) We download our results and mount it under `/mnt/secure_workflow`
