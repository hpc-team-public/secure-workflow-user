#!/bin/bash
#SBATCH -p <secure_partition>
#SBATCH -t 01:00:00
#SBATCH --nodelist=<prepared_node>
#SBATCH --exclusive=user

source /etc/profile

module load singularity


# First we mount our two data Containers
# We have 2 options: Either we are doing it in here, or we ssh seperately onto the node and moutn and umount it manually. Have a look at execute_me to see how it is done.
mount_data test
mount_data testoutput

# he second example converts a png to a 20% sized jpg image. It assumes that the corresponding source.png file is in your data container at the beginning. It also assumes, that you have two data containers mounted, i.e. test and test_output.  Afterwards you can download your test_output container again and you will find a shrinked dest.jpg in it.
singularity run --bind /data/$(whoami):/data  --pem-path=/keys/users/$(whoami)/rsa_pri.pem secure_workflow/encrypted.sif bash -c "convert -resize 20% /data/test/source.png /data/testoutput/dest.jpg"

# In order to leave the compute node securly we umount our data containers. Have a look above at mount_data.
umount_data test
umount_data testoutput
