# Introduction

This is an instruction for an secure workflow on the GWDG Scintific Compute Cluster. It is divided into the following steps: At first you create n-many encrypted LUKS Data Container. Your software goes into an encrypted Singularity Container. 
The encrypted data and Singularity Container get uploaded into you `$HOME` and your keys must only go into an secure mount on a secured node (use `secure_upload.sh`).

**Please read the following instrcutions carefully**

# prerequisites

Singularity > 3.4.0

Cryptsetup > 2.0.0

GWDG-Account activated for Scientific Computing

A Secure Node from your Admin

# Create a LUKS Data Container
In order to create a [LUKS](https://wiki.ubuntuusers.de/LUKS/) data container you can simply execute `create_data_container.sh` with:
```
sudo create_data_container.sh <container_name> <mount_path> <size>
``` 
Here, `<mount_path>` is the mount path on your local system where the container will be mounted. `<size>` expects the size of the data container in MB to the base of 10. The `<container_name>` will be the name of the data container, without any file extension.
After executing it, you will be ask to hit enter. Before hitting enter, the container will be available on your local system under `<mount_path>`. You can now copy your data into the data container. Now you need umount your container and close the device:
```
sudo create_data_container_umount.sh <container_name> <mount_path>
```
In your current working directory, you will see that you have a <container_name>.img file containing your encrypted data and a <container_name>.key file which is the key file used to encrypt and decrypt your data.
**Please be very catious where you put your <container_name>.key, since your data will be compromised if the key falls into the wrong hands**

# Create an encrypted Singularity Container

In order to create an encrypted Singularity container, please follow the official instructions here: https://sylabs.io/guides/3.4/user-guide/encryption.html
It basically boils down to:
1) Create a PEM file from an RSA key pair with: 
``` 
ssh-keygen -t rsa -b 4096 -m pem
ssh-keygen -f ./rsa.pub -e -m pem >rsa_pub.pem
mv rsa rsa_pri.pem # Just to rename your private Key
```
In this example we created a new dedicated key pair and created a PEM file from it. Here we assume, that the key was stored in the current workign directory with the name `./rsa`

2) Then build your container with 
```
sudo singularity build --pem-path=rsa_pub.pem encrypted.sif recipe
```
3) To run this container you can use
```
singularity run --pem-path=rsa_pri.pem encrypted.sif
```
If you would like to quickly test this, you can find an simple recipe in `Example_1/`. This Singularity container can also be used later in order to compute the example job presented in step 4 of "Prepare your Node on the HPC System and Execute Your Job".

# Upload Data

Since your data container is now encrypted you can upload it for instance in your `$HOME` with:
```
scp <container_name>.img <hpc_user_name>@transfer-scc.gwdg.de:/usr/users/<hpc_user_name>/
# or with a changed ssh config
scp <container_name>.img transfer-scc.gwdg.de:/usr/users/<hpc_user_name>/
```
**Please remember: Your `$HOME` is not a safe place. Never upload your key there.**

You can do the same with your encrypted Singularity container.

# Prepare your Node on the HPC System and Execute Your Job
**In the following steps you will be uploading your data and your keys onto the HPC system, however into different parts (storage/memory) of it. If you have any fundamental problem of understanding it, please ask us for help. Not following these precise instructions can compromise you data**

Only after a compute node has been secured by an admin, it is safe for you to upload your keys, both the PEM file of your singularity container and your .key file for you LUKS data container, onto it. For this reason, you should use the provided `secure_upload.sh` script. It will check the state of the compute node and only if it is secured it will upload your keys into **`/keys/users/<user_name>`. This is the only place, where your keys are safe.**
The following instructions assume, that you have prepared you ssh config:
```
Host gwdu101
        User <user_name>
        Hostname gwdu101.gwdg.de
        IdentityFile ~/.ssh/<your_private_key_for_the_cluster>
        ForwardAgent yes

Host <secure_node_name_1>
        User <user_name>
        HostName <secure_node_name_1>
        IdentityFile ~/.ssh/<your_private_key_for_the_secure_workflow>
        ProxyJump <user_name>@gwdu101.gwdg.de
        ForwardAgent yes

Host <secure_node_name_2>
        User <user_name>
        HostName <secure_node_name_2>
        IdentityFile ~/.ssh/<your_private_key_for_the_secure_workflow>
        ProxyJump <user_name>@gwdu101.gwdg.de
        ForwardAgent yes
```
Now, after an admin has secured a node for you, you can do the following steps to prepare the node for you:
1) ssh to a frontend (e.g. `ssh gwdu101`) and submit a dummy job to SLURM (Have a look at `run_dummy_job.sh`):
```
sbatch run_dummy_job.sh
```
2) From your own system go to your folder with your data container and key and do:
```
secure_upload.sh <secure_node> <container_name.key or rsa_pri.pem> <hpc_user_name>
```
As indicated, you can also upload your PEM file with this method.

3) Now cd to your data container and either do 
```
ssh <secure_node>
mount_data <container_name>
```
Please consider that `<container_name>` is without the `.img`.
or you can put 
```
mount_data <container_name>
```
in your run script for your job. This, of course, requires that you uploaded your key beforehand.
**Important: The mount script on the Compute Cluster differs from the one provided here. It will only look for your data containers in `~/secure_workflow/`. Please make sure to upload your data Containers to that directory.**


4) Submit your Job to your prepared compute node.
```
sbatch run.sh
```
You find an example `run.sh`  in `Example_1/` where a simple test.txt is created and written to in the encrypted data container. Herefore, we bind mounted the encrypted data container under `/data` into your singularity container. The `<container_name>` was here `test`.
The second example, also in the `run.sh` converts a png to a 20% sized jpg image. It assumes that the corresponding `source.png` file is in your data container at the beginning. Afterwards you can download your container again and you will find a shrinked `dest.jpg` in it. This example assumes that you have two data containers mounted, one for the input image and another one for the output image. We recommend this in practice since you will spare a lot of data movement and you can reuse your input data container, if you don't write to it.

**For security reasons please only execute encrypted singularity containers. Feel free to pgp - sign them too.** 

5) Once you are done, you can umount your data container with 
```
umount_data <container_name>
```
Now the data container is unmounted. If you still have a running job on this node relying on this mount, it will crash.
You can also include this line at the end of your run script.
