#!/bin/bash

container_name=$1
mount_path=$2

if ! [[ $container_name =~ ^[a-zA-Z1-9]+$ ]]; then
    echo 'Wrong Container Name. Only a-zA-Z and numbers 1-9 are allowed' >&2
    exit 1
fi

user_name=$(whoami)
key_file=${container_name}.key
container_file=${container_name}.img

sudo cryptsetup luksOpen $container_file $container_name --key-file $key_file

sudo mkdir -p $mount_path/$container_name

sudo mount -t ext4 /dev/mapper/$container_name $mount_path/$container_name

sudo chown $user_name $mount_path/$container_name
sudo chmod 755 -R $mount_path/$container_name

echo "Data is mounted under $mount_path/$container_name"
