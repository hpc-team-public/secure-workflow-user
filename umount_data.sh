#!/bin/bash 

container_file=$1
mount_path=$2

sudo umount $mount_path/"$container_file"

sudo cryptsetup luksClose "$container_file"

tmp=$(sudo losetup -a | grep "/${container_file}.img" | awk '{print $1}')
for elem in $tmp ; do 
    echo "${elem%?}"
    sudo losetup -d "${elem%?}" ;
done
