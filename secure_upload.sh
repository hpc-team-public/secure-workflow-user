#!/bin/bash

if [ "$1" == "-h" -o "$1" == "--help" ]; then
    echo "Program used to securely upload your key files to the secured compute nodes. Usage: secure_upload.sh <secure_node_name> <full_filename> <hpc_user_name> .  Here, <full_filename> expects the full filename of your key file like test.key or or id_rsa.pem."
    exit 0
fi

if [[ $(ssh $1 'if [[ $(ls / | grep keys) ]]; then echo "Node secured"; fi') ]]; then
    if [[ $(ssh $1 'if [[ $(df -T /keys | grep tmpfs) ]]; then echo "Node secured"; fi')  ]]; then
        scp $2 $1:/keys/users/$3/ ;
    fi
fi
